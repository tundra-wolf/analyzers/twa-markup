# README for the Documentation Markup Analysis Plugin (package `twa-markup`)

This crate includes the parsers and analyzers for a number of documentation markup languages. The crate parses the document into a source document structure, that can be separately analysed. Via the feature gates the translation to the Tundra Wolf semantic representation is also availabe.

## Supported Formats

The following document formats (and potentially more if needed) are supported by this library. Initially only the ones marked with * are included as those are the most relevant working with this project.

* AsciiDoc*
* DocBook
* Markdown*
* Open Document Format (odf)
* Office Open XML (ooxml)
* Tex / LaTeX
* Troff
