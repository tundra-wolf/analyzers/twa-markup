# Documentation Markup Analyzer - Specs

## AsciiDoc

There is no standard yet, but one is in the making at [AsciiDoc Language Project](https://gitlab.eclipse.org/eclipse/asciidoc-lang/asciidoc-lang/-/tree/main/docs).

Until that specification is finished the [AsciiDoctor Language Documentation](https://docs.asciidoctor.org/asciidoc/latest/) website is used for implementing the parser/analyzer.

## DocBook

* [DocBook 5.x](https://docbook.org/specs/) on the DocBook website

## Markdown

* [Basic Syntax](https://www.markdownguide.org/basic-syntax/)

* [Extended Syntax](https://www.markdownguide.org/extended-syntax/)

    * [CommonMark](https://commonmark.org/)
    * [GFM](https://github.github.com/gfm/)
    * [Multimarkdown](https://fletcherpenney.net/multimarkdown/)
    * [R Markdown](https://rmarkdown.rstudio.com/index.html)

## Open Document Format

* [Open Document Format 1.3](https://docs.oasis-open.org/office/OpenDocument/v1.3/) on Oasis Open

## Office Open XML

* [Office Open XML, 5th edition](https://ecma-international.org/publications-and-standards/standards/ecma-376/) on the ECMA standards website

## TeX/LaTeX

* A full description of TeX and METAFONT can be found in the book series "Computers & Typesetting" consisting of the following volumes.

    * Volume A, The TeXbook (Reading, Massachusetts: Addison-Wesley, 1984)
    * Volume B, TeX: The Program (Reading, Massachusetts: Addison-Wesley, 1986)
    * Volume C, The METAFONTbook (Reading, Massachusetts: Addison-Wesley, 1986)
    * Volume D, METAFONT: The Program (Reading, Massachusetts: Addison-Wesley, 1986)
    * Volume E, Computer Modern Typefaces (Reading, Massachusetts: Addison-Wesley, 1986)

* A full description of LaTeX can be found in the book series "The LaTeX Companion, 3rd edition".

## TROFF

There is no standard for TROFF. The [Troff.org](https://www.troff.org/papers.html) website contains some papers on the TROFF language and it's preprocessors.

The following books contains all details on the language and it's usage.

* Document Formatting and Typesetting on the UNIX System, 2nd Ed., Narain Gehani
* Document Formatting and Typesetting on the UNIX System Volume II: grap, mv, ms, and troff, Narain Gehani, Steven Lally
* UNIX Text Processing, Dale Dougherty, Tim O'Reilly

The GNU package Groff is a modern implementation of the TROFF language. The [manual](https://www.gnu.org/software/groff/manual/groff.html.node/) is available on the GNU website.
